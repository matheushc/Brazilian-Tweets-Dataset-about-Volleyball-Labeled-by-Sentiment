## Brazilian Tweets Dataset about Volleyball Labeled by Sentiment

### Authors
Matheus Henrique Cardoso, Anita Maria da Rocha Fernandes

### Affiliations
University of Itajaí Valley – UNIVALI. Santa Catarina – Brazil.

### Data Description
This database contains Brazilian Portuguese phrases about volleyball and their respective sentiment polarity. All sentences were manually labeled by the author with negative, neutral and positive polarities. The dataset contains 12.506 texts, where 1.032 negative, 10.176 neutral and 1.298 positive sentiment polarity.

The polarities are represented as:
* Negative: -1;
* Neutral: 0;
* Positive: 1.

These texts were processed with the following steps:
* Removing stopwords;
* Removing numbers;
* Removing symbols (#, @, %, &, !, etc...);
* Removing line break;
* Removing URLs, Links and mentions;
* Removing punctuation;
* Transformed to lowercase.

These dataset were developed with texts extracted from Twitter in order to be used to train and test classification models of Sentiment Analysis in the portuguese language used in Brazil. In order to organize the data, this is represented in CSV (Comma-separated values) format.

![Sample](sample.png)

### Corresponding author(s)
[Matheus Cardoso](mailto:matheus.cardoso@edu.univali.br)
